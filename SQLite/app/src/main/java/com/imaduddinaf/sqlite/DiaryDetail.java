package com.imaduddinaf.sqlite;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DiaryDetail extends ActionBarActivity implements android.view.View.OnClickListener{

    Button btnSave ,  btnDelete;
    Button btnClose;
    EditText editTextJudul;
    EditText editTextDeskripsi;
    private int _Diary_Id=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diary_detail);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnClose = (Button) findViewById(R.id.btnClose);

        editTextJudul = (EditText) findViewById(R.id.editTextJudul);
        editTextDeskripsi = (EditText) findViewById(R.id.editTextDeskripsi);

        btnSave.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnClose.setOnClickListener(this);


        _Diary_Id =0;
        Intent intent = getIntent();
        _Diary_Id =intent.getIntExtra("diary_Id", 0);
        DiaryRepo repo = new DiaryRepo(this);
        Diary diary = new Diary();
        diary = repo.getDiaryById(_Diary_Id);

        editTextJudul.setText(diary.judul);
        editTextDeskripsi.setText(diary.deskripsi);
    }



    public void onClick(View view) {
        if (view == findViewById(R.id.btnSave)){
            DiaryRepo repo = new DiaryRepo(this);
            Diary diary = new Diary();
            diary.judul = editTextJudul.getText().toString();
            diary.deskripsi = editTextDeskripsi.getText().toString();
            diary.diary_ID = _Diary_Id;

            if (_Diary_Id==0){
                _Diary_Id = repo.insert(diary);

                Toast.makeText(this, "New Diary Insert", Toast.LENGTH_SHORT).show();
            }else{

                repo.update(diary);
                Toast.makeText(this,"Diary Record updated",Toast.LENGTH_SHORT).show();
            }
        }else if (view== findViewById(R.id.btnDelete)){
            DiaryRepo repo = new DiaryRepo(this);
            repo.delete(_Diary_Id);
            Toast.makeText(this, "Diary Record Deleted", Toast.LENGTH_SHORT);
            finish();
        }else if (view== findViewById(R.id.btnClose)){
            finish();
        }


    }

}