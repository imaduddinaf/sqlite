package com.imaduddinaf.sqlite;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends ListActivity implements android.view.View.OnClickListener{

    Button btnAdd,btnGetAll;
    TextView diary_Id;

    @Override
    public void onClick(View view) {
        if (view== findViewById(R.id.btnAdd)){

            Intent intent = new Intent(this,DiaryDetail.class);
            intent.putExtra("diary_Id",0);
            startActivity(intent);

        }else {
            RefreshList();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        btnGetAll = (Button) findViewById(R.id.btnGetAll);
        btnGetAll.setOnClickListener(this);

        RefreshList();
    }

    private void RefreshList(){

        DiaryRepo repo = new DiaryRepo(this);

        ArrayList<HashMap<String, String>> diaryListList =  repo.getDiaryList();
        if(diaryListList.size()!=0) {
            ListView lv = getListView();
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                    diary_Id = (TextView) view.findViewById(R.id.diary_Id);
                    String diaryId = diary_Id.getText().toString();
                    Intent objIndent = new Intent(getApplicationContext(),DiaryDetail.class);
                    objIndent.putExtra("diary_Id", Integer.parseInt(diaryId));
                    startActivity(objIndent);
                }
            });
            ListAdapter adapter = new SimpleAdapter( MainActivity.this,diaryListList, R.layout.view_diary_entry, new String[] { "id","judul"}, new int[] {R.id.diary_Id, R.id.diary_judul});
            setListAdapter(adapter);
        }else{
            Toast.makeText(this, "No diary!", Toast.LENGTH_SHORT).show();
        }

    }

    //edit
}