package com.imaduddinaf.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by imaduddinaf  : imaduddinalfikri on 05-Dec-15.
 */
public class DBHelperTes extends SQLiteOpenHelper {
    //version number to upgrade database version
    //each time if you Add, Edit table, you need to change the
    //version number.
    private static final int DATABASE_VERSION = 4;

    // Database Name
    private static final String DATABASE_NAME = "crud2.db";

    public DBHelperTes(Context context ) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //All necessary tables you like to create will create here

        String CREATE_TABLE_DIARY = "CREATE TABLE " + Diary.TABLE  + "("
                + Diary.KEY_ID  + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + Diary.KEY_judul + " TEXT, "
                + Diary.KEY_deskripsi + " TEXT )";

        db.execSQL(CREATE_TABLE_DIARY);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed, all data will be gone!!!
        db.execSQL("DROP TABLE IF EXISTS " + Diary.TABLE);

        // Create tables again
        onCreate(db);

    }

}
