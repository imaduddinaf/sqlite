package com.imaduddinaf.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by imaduddinaf  : imaduddinalfikri on 05-Dec-15.
 */
public class DiaryRepo {
    private DBHelperTes dbHelper;

    public DiaryRepo(Context context) {
        dbHelper = new DBHelperTes(context);
    }

    public int insert(Diary diary) {

        //Open connection to write data
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Diary.KEY_judul, diary.judul);
        values.put(Diary.KEY_deskripsi,diary.deskripsi);

        // Inserting Row
        long diary_Id = db.insert(Diary.TABLE, null, values);
        db.close(); // Closing database connection
        return (int) diary_Id;
    }

    public void delete(int diary_Id) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // It's a good practice to use parameter ?, instead of concatenate string
        db.delete(Diary.TABLE, Diary.KEY_ID + "= ?", new String[] { String.valueOf(diary_Id) });
        db.close(); // Closing database connection
    }

    public void update(Diary diary) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Diary.KEY_judul, diary.judul);
        values.put(Diary.KEY_deskripsi,diary.deskripsi);

        // It's a good practice to use parameter ?, instead of concatenate string
        db.update(Diary.TABLE, values, Diary.KEY_ID + "= ?", new String[] { String.valueOf(diary.diary_ID) });
        db.close(); // Closing database connection
    }

    public ArrayList<HashMap<String, String>> getDiaryList() {
        //Open connection to read only
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery =  "SELECT  " +
                Diary.KEY_ID + "," +
                Diary.KEY_judul + "," +
                Diary.KEY_deskripsi +
                " FROM " + Diary.TABLE;

        //Student student = new Student();
        ArrayList<HashMap<String, String>> diaryList = new ArrayList<HashMap<String, String>>();

        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> diary = new HashMap<String, String>();
                diary.put("id", cursor.getString(cursor.getColumnIndex(Diary.KEY_ID)));
                diary.put("judul", cursor.getString(cursor.getColumnIndex(Diary.KEY_judul)));
                diaryList.add(diary);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return diaryList;

    }

    public Diary getDiaryById(int Id){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery =  "SELECT  " +
                Diary.KEY_ID + "," +
                Diary.KEY_judul + "," +
                Diary.KEY_deskripsi +
                " FROM " + Diary.TABLE
                + " WHERE " +
                Diary.KEY_ID + "=?";// It's a good practice to use parameter ?, instead of concatenate string

        int iCount =0;
        Diary diary = new Diary();

        Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(Id) } );

        if (cursor.moveToFirst()) {
            do {
                diary.diary_ID =cursor.getInt(cursor.getColumnIndex(Diary.KEY_ID));
                diary.judul =cursor.getString(cursor.getColumnIndex(Diary.KEY_judul));
                diary.deskripsi  =cursor.getString(cursor.getColumnIndex(Diary.KEY_deskripsi));

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return diary;
    }

}
