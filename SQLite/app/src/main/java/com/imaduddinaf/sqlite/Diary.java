package com.imaduddinaf.sqlite;

/**
 * Created by imaduddinaf  : imaduddinalfikri on 05-Dec-15.
 */
public class Diary {
    // Labels table name
    public static final String TABLE = "Diary";

    // Labels Table Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_judul = "judul";
    public static final String KEY_deskripsi = "deskripsi";

    // property help us to keep data
    public int diary_ID;
    public String judul;
    public String deskripsi;

}
